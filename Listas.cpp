#include<iostream>
#include<string>

using namespace std;

class Estudiante{
    private: 
        string nombre;
        int edad;

    public:
        Estudiante(string nombre, int edad){
            this->nombre = nombre;
            this->edad = edad;
        }

        Estudiante(){
            this->nombre = "";
            this->edad = 0;
        }

        string getNombre(){
            return nombre;
        }

        void setNombre(string nombre){
            this->nombre = nombre;
        }   

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};


//Por comodidad, es mejor definir la clase "Nodo" para luego definir la lista
//(cada nodo esta formado por los datos y por su "apuntador" al siguiente)
class NodoEstudiante{
    private:
        Estudiante valor;
        NodoEstudiante *vecino;

    public:
        NodoEstudiante(){
            this->vecino = NULL;
        }

        NodoEstudiante(Estudiante nuevo){
            this->valor.setNombre(nuevo.getNombre());
            this->valor.setEdad(nuevo.getEdad());

            this->vecino = NULL;
        }

        void setVecino(NodoEstudiante *vec){
            this->vecino = vec;
        }
                
        NodoEstudiante *getVecino(){
            return(this->vecino);
        }

        Estudiante *getEstudiante(){
            return(&this->valor);
        }

};

class ListaEstudiantes{
    private:
        NodoEstudiante *primero, *ultimo;

    public:
        ListaEstudiantes(){
            primero = ultimo = NULL;
        }

        void insertarNodo(NodoEstudiante *nuevo){
            if (ultimo == NULL){
                primero = ultimo = nuevo;
            }else{
                ultimo->setVecino(nuevo);
                ultimo = nuevo;
            }            
        }

        void mostrarLista(){
            NodoEstudiante *recorrer;

            recorrer = primero;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                cout <<"Estudiante" << endl;
                cout <<"-- Nombre: " << estudiante->getNombre()  << endl;;
                cout <<"-- Edad: " << estudiante->getEdad() << endl;

                recorrer = recorrer->getVecino();
           }
        }
        void buscarEstudiante(string nombre){
            NodoEstudiante *recorrer;

            recorrer = primero;
            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();
            }
        }
        
        void insertarPrimero(NodoEstudiante *nuevo){

        }

        void quitarUltimo(){

        }
        
        void quitarPrimero(){

        }


};

int main(){    
    ListaEstudiantes lista;
    Estudiante p("Estudiante 1", 20);
    NodoEstudiante *nuevo= new NodoEstudiante(p);

    lista.insertarNodo(nuevo);
    lista.mostrarLista();

    Estudiante q("Estudiante 2", 30);
    NodoEstudiante *nuevo2= new NodoEstudiante(q);

    lista.insertarNodo(nuevo2);
    lista.mostrarLista();

    return 0;
}
